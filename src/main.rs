#[macro_use] extern crate serenity;
extern crate image;

use serenity::client::Client;
use serenity::prelude::EventHandler;
use serenity::framework::standard::StandardFramework;
use std::{ thread, time };
use std::time::Instant;
use std::io::Cursor;
use image::GenericImage;
use image::GenericImageView;

struct Handler;

impl EventHandler for Handler {}

fn main() {
    // Login with a bot token from the environment
    let mut client = Client::new(include_str!("../token"), Handler)
        .expect("Error creating client");
    client.with_framework(StandardFramework::new()
        .configure(|c| c.prefix("~")) // set the bot's prefix to "~"
        .cmd("ping", ping)
        .cmd("blurplefy", blurple));

    // start listening for events by starting a single shard
    if let Err(why) = client.start() {
        println!("An error occurred while running the client: {:?}", why);
    }
}

command!(ping(_context, message) {
    thread::sleep(time::Duration::from_millis(5000));

    let my_str = format!("Pong! Thread {:?}", thread::current().id());
    let _ = message.reply(&my_str[..]);
});

command!(blurple(_context, message) {

    let start = Instant::now();
    for attachment in &message.attachments {
        match attachment.download() {
            Ok(mut buf) => {
                let start = start.elapsed().as_millis();
                let download = Instant::now();
                match image::guess_format(&buf) {
                    Ok(format) => {
                        match image::load(Cursor::new(buf), format) {
                            Ok(mut img) => {
                                let download = download.elapsed().as_millis();
                                let process = Instant::now();

                                
                                for (x, y, mut pixel) in img.grayscale().pixels() {
                                    let val = pixel[0];
                                    let lower = (255.0 * 0.33) as u8;
                                    let upper = (255.0 * 0.66) as u8;

                                    let color = {
                                        if val < lower {
                                            vec![78, 93, 148] // Dark blurple
                                        } else if val < upper {
                                            vec![114, 137, 218] // Blurple
                                        } else {
                                            vec![255, 255, 255] // White
                                        }
                                    };

                                    pixel[0] = color[0];
                                    pixel[1] = color[1];
                                    pixel[2] = color[2];
                                    img.put_pixel(x, y, pixel);
                                }
                                let mut buf: Vec<u8> = vec![];
                                match img.write_to(&mut buf, image::ImageFormat::PNG) {
                                    Ok(_) => {
                                        let process = process.elapsed().as_millis();
                                        let upload = Instant::now();
                                        let channel = message.channel().unwrap();
                                        match channel.send_files(vec![(&buf[..], "blurple.png")], |m| m.content("foo")) {
                                            Ok(_) => {
                                                let upload = upload.elapsed().as_millis();
                                                let reply = format!("`{}`ms download, `{}`ms process, `{}`ms upload", download, process, upload);
                                                match channel.send_message(|m| m.content(&reply)) {
                                                    Ok(_) => {}
                                                    Err(e) => {
                                                        println!("{}", e);
                                                    }
                                                }
                                            }
                                            Err(e) => {
                                                let _ = message.reply(&format!("Whoops: {}", e));
                                            }
                                        }
                                    }
                                    Err(e) => {
                                        let _ = message.reply(&format!("Whoops: {}", e));
                                    }
                                }
                            }
                            Err(e) => {
                                let _ = message.reply(&format!("Whoops: {}", e));
                            }
                        }
                    }
                    Err(e) => {
                        let _ = message.reply(&format!("Can't determine image format: {}", e));
                    }
                }
            }
            Err(e) => {
                let _ = message.reply(&format!("Whoops: {}", e));
            }
        }
    }
});